
# Week 1: Project Management

![Screenshot](img/profile pic.png)


# About Me..
Hi I'm Yael, am 22 years old and am REALLY interested in ICT.
From a young age on I showed interest in ICT.
The first things I did were drawings in windows paint.
My brother saw the potential in me and started investing in me. Giving me laptops to work with, making sure I had Adobe etc. etc.


# Objectives
- [x] Write down my about.
- [x] Research my final project
- [x] Basic documentation outline
- [x] Document in pm.md project management (what was done so far)

# Research on final project
For my final project I plan on making a StudyBuddy., I got this idea while attending a motivational session from school.
These days the fact that students get distracted while studying is a common problem, thats why I want to make this fun yet helpful product to help people
acomplish their goals and study the way they should. The buddy is designed so that every time you complete your homework or any task for school,
you can check it off. It has a timer so you can set in the amount of time you want to spend studying a certain subject,
and it has a speaker for those who study with music. It will also have a color changing light and aroma reservoir to help the person relax. 

## Visualization of my final project
**StudyBuddy**

![Screenshot](img/Study Buddy.jpg)



