For my final project I am making an automated fishfeeder.

The reason I chose this projet is because of some difficulties we incounter in everyday life.

Some of the reasons are:
-Big Aquaponics farms that need automated systems to feed their fish, because if you skip one feeding it can be very bad for your fish.. They can die.
-Households where people are often away from home and are therefore looking for a solution for the fish in their aquarium or pond just so that the fish are fed on time..
-Restaurants/Shops where employees and holders don’t always have the time to look after fish00

The sollution I came up with is the automated feeder

Some of the priveleges the automated feeder gives are:
-Easy fish feeding (Just by pressing a button or installing a timer)
-Measuring the amount of food the fish get
-And getting notified when the food in the reservoir is running low..
