## Project Management
**Project Management**

### Week 1  

**GitLab clone**  
  First I had to Register online (GitLab and GitHub) and then clone my repository.  
![Screenshot](../img/6.png)  

**GitHub Desktop**    
![Screenshot](../img/16.png)  

**Local File system**  
![Screenshot](../img/1.png)  
  
**Change made in notepad++**  
I started off by building my webpage online. When the server crashed and I lost everything I already did 
I started over in notepad++, I put in pictures of me and my final project.  
![Screenshot](../img/2.png)  
  
**GitHub desktop auto change detection + commit  text**    
![Screenshot](../img/3.png)   
 
**Git commit**   
Synced my local and my online with eachother  
![Screenshot](../img/4.png)  
  
**Git push**  
Edited my about page in notepad++, uploaded it to my GitHub and then pushed it to my GitLab  
![Screenshot](../img/5.png)  
  
**Gitlab change made**   
![Screenshot](../img/8.png)  
  

