# Week 6
  
## Configuring Raspberry pie
  
First we have to know what the Raspberry pie is. The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse.   
It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.   
It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video, to making spreadsheets, word-processing, and playing games.  
![Screenshot](../img/iap/1.png)
  
### Raspberry Pi 2 features and specifications
  
CPU: Broadcom BCM2836 900MHz quad-core ARM Cortex-A7 processor  
RAM: 1 GB SDRAM  
USB Ports: 4 USB 2.0 ports  
Network: 10/100 Mbit/s Ethernet  
Power Ratings: 600 mA (3.0 W)  
Power Source: 5V Micro USB  
Size: 85.60 mm × 56.5 mm  
Weight: 45 g
  
### Configuring your SD-Card for your RaspberryPi
First thing, get new sd card. Than you insert the SD-Card in your SD-Card port. Download win32diskimager (on your computer), which we will  use to configer the SD-Card.
*Win32diskimager: This program is designed to write a raw disk image to a removable device or backup a removable device to a raw image file.*
*It is very useful for embedded development, namely Arm development projects (Android, Ubuntu on Arm, etc).*
Look for the jessie image on your laptop and write it onto the SD-Card.
This will write the image (software) into the SD-Card.
**Note: Whatever you do DON'T format the SD-Card if it prompt up on your laptop**
  
## What to check after configuring the SD Card with your PC
After configuring the SD Card, you will find a Boot-drive on your PC. 
Open this and then, open cmdline.txt in notepad ++ to hardcoded the adress of your RspberryPi. Mine is 192.168.1.156.
You have to do so because it is needed to install or work with putty. Go to your SD-card drive and make a new text file, name it SSH, without the text extention.
You have to do so to allow putty to access the Raspberry Pi remotely.

## Crosscable  
crossing over:  
A crosscable allows you to connect your raspberry to a computer thinking it connects to a hub or switch, this because you cross the cables.   
![Screenshot](../img/iap/cc.png) 


An ordinary patch cable connects different types of devices, for example, a computer and a network switch. A crossover cable connects two devices of the same type.
The ends of a patch cable can be wired in any way as long as both ends are identical. Compared to straight-through Ethernet cables, the internal wiring of a crossover cable reverses the transmit and receive signals.
Standard cables have an identical sequence of colored wires on each end.
Crossover cables have the first and third wires (counting from left to right) crossed, as well as the second and sixth.
  
## Installing needed Applications
  
**1: Nodejs**  
Node.js is an open-source server and cross-platform JavaScript runtime environment. It is a popular tool for almost any kind of project!
And it's free.  
Node.js runs the V8 JavaScript engine, the core of Google Chrome, outside of the browser. This allows Node.js to be very performant.  
A Node.js app is run in a single process, without creating a new thread for every request. Node.js provides a set of asynchronous I/O primitives in its standard library that prevent JavaScript code from blocking and generally, 
libraries in Node.js are written using non-blocking paradigms, making blocking behavior the exception rather than the norm.  
When Node.js needs to perform an I/O operation, like reading from the network, accessing a database or the filesystem, instead of blocking the thread and wasting CPU cycles waiting, 
Node.js will resume the operations when the response comes back.  
This allows Node.js to handle thousands of concurrent connections with a single server without introducing the burden of managing thread concurrency, which could be a significant source of bugs.
Node.js runs on various platforms (Windows, Linus, Unix, Mac OS X, etc.)  
JavaScript is thegramming Language for the web, it can update and change both HTML and CSS. It can calculate, manipulate and validate data.  
Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

### Some of the Basic Linus Commands are:

cd : to go to a directory  
cd ..: to go back to your last directory  
touch: to create or update a file  
mkdir: to cfreate a Directory  
ls: To see whats in your directory (folder)  
rm: To remove a file  
rmdir: To remove a folder  
*With these commands you can configurate the raspberryPi using Putty.exe*

Note:  
Nodejs and Python are two different Website servers
To check our version on our RaspberryPi we use: Nodejs __version , python __version , pip __version
Use the npm to download your nodejs dependencies (libaries) and for python we use pip version. 1.5.6

**What is npm?**  
npm *(originally short for Node Package Manager)* is a package manager for the JavaScript programming language. 
It is the default package manager for the JavaScript runtime environment Node.js. It consists of a command line client, also called npm, and an online database of public and paid-for private packages, called the npm registry. 
The registry is accessed via the client, and the available packages can be browsed and searched via the npm website. The package manager and the registry are managed by npm, Inc.
*Express is the app for Nodejs and Flask is the app for python.*
When using these apps, modules and libaries are running in the background.
Modules are showing us the right path, which lies in the public folder (the route) for nodjes and the templates folder for python.
*JavaScript* is the Programming Language for the *Web*, it can update and change both *HTML and CSS* (installfile sheets) and it can calculate, manipulate and validate data.
And the language we are using for *Python* is *python*.

### Installing Nodejs Server:
Open putty and type in the following commands:

1: node --version (to see the version of nodejs first)  
2: curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - sudo apt-get install nodejs (to install the right version of nodejs)  
3: go to index.js open it and edit it  

![Screenshot](../img/iap/3.png)

• go to the project1 folder and type npm space init space -y  
• npm i D nodemon (to install nodemon)  
• integrate npm init -y  
• you should have your structure in your index.js after you did that you have to put code in it  
• npm i express (i means install)  
• open package.json and change "test..." to " "dev": "nodemon ./index.js"  

**Must know!!**  
sudo npm install means it looks for everything in your package.json using the superior rights (it's like having admin rights)

![Screenshot](../img/iap/4.png)

All npm packages contain a file, usually in the project root, called package.json - this file holds various metadata relevant to the project.
This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies.
It can also contain other metadata such as a project description, the version of the project in a particular distribution, license information, even configuration data - all of which can be vital to both npm and to the end users of the package.
The package.json file is normally located at the root directory of a Node.js project.

Open index.js and edit some more:  
*Code below*

		// index.js

		/**
		 * Required External Modules
		 */

		const express = require("express");
		const path = require("path");

		/**
		 * App Variables
		 */

		const app = express();
		const port = process.env.PORT ||"8000";

		/**
		 *  App Configuration
		 */


		app.use(express.static(path.join(__dirname, "public")));

		/**
		 * Routes Definitions
		 */



		/**
		 * Server Activation
		 */

		app.listen(port, () => {
		  console.log(`Listening to requests on http://localhost:${port}`);
		});

Go to your public folder and open the index.html file then type these basic html-codes:

		<html>
		<body>
		<h1>My website</h1>
		</body>
		</html>

To start the nodejs Server type in : **npm run dev** in your putty console.
Open your browser and type in: http://192.168.1.156:8000/ and you can see your webpage using port 8000.

**Nodemon:**  
Nodemon is a utility that will monitor for any changes in your source and automatically restart your server. Perfect for development. Install it using npm.
Just use nodemon instead of node to run your code, and now your process will automatically restart when your code changes. To install, get node.js, then from your terminal run:  
![Screenshot](../img/iap/2.png)

**Python (*version 2.7.9*)**  
Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991. 
Python's design philosophy emphasizes code readability with its notable use of significant whitespace. 
Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.

**Flask** is a lightweight WSGI *(Web Server Gateway Interface)* web application framework. It was designed to make getting started with python quick and easy. With the ability to scale up to complex applications. 
It began as a simple wrapper around Werkzeug and Jinja and became one of the most popular Python web application frameworks.  
![Screenshot](../img/iap/1.png)

After making the folders and files, we open index.py and write the following codes:

		from flask import Flask, render_template

		app = Flask(__name__)

		@app.route('/')
		def index():
			return render_template('index.html')
		if __name__ == '__main__':
			app.run(debug=True, host='0.0.0.0')

### Explanation:
@app.route('/'): this determines the entry point; the / means the root of my website, so http://192.168.1.156:5000/
def index(): this is the name you give to the route; this one is called index, because it’s the index (or home page) of the website
which return my web page, when the user goes to this URL *(the address of a World Wide Web page)*.
When port is not given use port:8000

## Installing the Python Server:

Install python 2.7.9
Install flask using the command: *sudo pip install flask*
Go to your templates folder and open the index. html file and use some basic html-codes, javascript and css to make your HTML page.

**to run the Python Server type in: python index.py in your putty console**

*This was it for things about the raspberry pie*

# Arduino Uno Serial Communication

Serial communication is a connection between 2 devices over a cable.
In this case we are using The Arduino uno and the RaspberryPi.

**A little bit of serial knowledge**
In telecommunication and data transmission, serial communication is the process of sending data one bit at a time, sequentially, over a communication channel or computer bus. 
This is in contrast to parallel communication, where several bits are sent as a whole, on a link with several parallel channels.
Serial communication is used for all long-haul communication and most computer networks, where the cost of cable and synchronization difficulties make parallel communication impractical. 

## Installing the Arduinno uno to our RaspberryPi:

1: Open putty and type: sudo pip install pyserial (we need to have the serial libary)  
2: connect your Arduino Uno to the RaspberryPi  
3: type lsub to see a list of your usbports and on which your Arduino UNO is connected  
4: type in: dmesg | grep tty to see the serial port for your RaspberryPi(/dev/ttyAMA0  (for raspberry pi 2))  
5: Edit your index.py using the following code:  

	ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
	def led_on_off():
		user_input = input("\n Type on / off / quit : ")
		if user_input =="on":
			print("LED is on...")
			time.sleep(0.1) 
			ser.write(b'H') 
			led_on_off()
		elif user_input =="off":
			print("LED is off...")
			time.sleep(0.1)
			ser.write(b'L')
			led_on_off()
		elif user_input =="quit" or user_input == "q":
			print("Program Exiting")
			time.sleep(0.1)
			ser.write(b'L')
			ser.close()
		else:
			print("Invalid input. Type on / off / quit.")
			led_on_off()

	time.sleep(2) # wait for the serial connection to initialize

	led_on_off()
	
Go to your pythonprojectsfolder and type sudo pyton index.py (do this in your putty)

# HTML

## HTML5
HTML5 is a markup language used for structuring and presenting content on the World Wide Web.
It is the fifth and last major version of HTML that is a World Wide Web Consortium (W3C) recommendation. 
The current specification is known as the HTML Living Standard and is maintained by a consortium of the major browser vendors (Apple, Google, Mozilla, and Microsoft), 
the Web Hypertext Application Technology Working Group (WHATWG).
HTML5 was first released in public-facing form on 22 January 2008, with a major update and "W3C Recommendation" status in October 2014. 
Its goals were to improve the language with support for the latest multimedia and other new features; 
to keep the language both easily readable by humans and consistently understood by computers and devices such as web browsers, parsers, etc., without XHTML's rigidity; and to remain backward-compatible with older software. 
HTML5 is intended to subsume not only HTML 4, but also XHTML 1 and DOM Level 2 HTML.
It also includes detailed processing models to encourage more interoperable implementations; it extends, improves and rationalizes the markup available for documents, 
and introduces markup and application programming interfaces (APIs) for complex web applications. For the same reasons, HTML5 is also a candidate for cross-platform mobile applications, 
because it includes features designed with low-powered devices in mind.

**Some resources about HTML5 technologies, classified into several groups based on their function.**  
1: Semantics: allowing you to describe more precisely what your content is.  
2: Connectivity: allowing you to communicate with the server in new and innovative ways.  
3: Offline and storage: allowing webpages to store data on the client-side locally and operate offline more efficiently.  
4: Multimedia: making video and audio first-class citizens in the Open Web.  
5: 2D/3D graphics and effects: allowing a much more diverse range of presentation options.  
6: Performance and integration: providing greater speed optimization and better usage of computer hardware.  
7: Device access: allowing for the usage of various input and output devices.  
8: Styling: letting authors write more sophisticated themes.  

**HTML is a scripting language, (Instruction tag)**  
**Beginn with <> and closed by </>**

### Some basic  tags:
	1. <B>FOR BOLD</B>  
	2. <H1> FOR HEADING1</H1>  
	3. <BR> FOR BREAK (go to the next line)  
	4. <img src="img/iot.jpg"/>  

### NEW Semantic TAGS that actually have more meaning to the standard
	<DIV> tags for layout.
	For layout : <article>, <header>, <footer>, <nav>, <aside>  
	For widgets: <meter>, <progress> etc

**Working with HTML5 we have a certain structure, in which the *root folder is the public folder* as well.**   
![Screenshot](../img/iap/5.png)

*Most of the HTML5 websites have the same structure:*

• Site Map  
• Folder Structure  
• Collect Context  
• Look & Feel/Layout  
• Mock up  
• Target Devices  
• Supported Browsers  

### How to create a basic HTML page?
Visit: https://www.w3schools.com/html/ (you can also use w3schools to learn css)
		**or**
Use following code:

	<!DOCTYPE html>
	<html>
	<head>
	   <meta charset="UTF-8">
	   <title>Title MyPage</title>
	</head>
	<body>
		<!-- body content starts here -->
		Here is the visible Content of the page......
	</body>
	</html>
	
### Create HTML webpage Layout with Divs and Sections  
**To do so use the following code:**

	<!DOCTYPE html>
	<html>

	<head>
		<meta charset="utf-8" />
		<title>Forest deforstation</title>
		<link href="css/style.css" rel="stylesheet" />
		<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
	</head>

	<body>
		<header Class = "mainHeader">
			<img src="img/logo.png">
			<nav>
				<ul>
					<li><a href="#" Class="active">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Portfolio</a></li>
					<li><a href="#">Gallery</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</nav>
		</header>

		<div Class="mainContent">
			<div Class="content">
				<article class="articleContent">
					<header>
						<h2>First Article #1</h2>
					</header>

					<footer>
						<p Class="post-info">Written by Super Woman</p>                 
					</footer>
					<content>
						<p> Deforestation is the permanent removal of trees to make room for something besides forest. 
						   This can include clearing the land for agriculture or grazing, or using the timber for fuel, construction or manufacturing. </p>
					</content>
				</article> 

				<article Class="articleContent">
					<header>
						<h2>2nd Article #2</h2>
					</header>

					<footer>
						<p class="post-info">Written by Super Woman</p>                 
					</footer>

					<content>
						<p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
					</content>
				</article> 

		   </div>
		</div>

		<aside Class="top-sidebar">
			<article>
				<h2>Top sidebar</h2>
				<p>Lorum ipsum dolorum on top</p>
			</article>
		</aside>

		<aside Class="middle-sidebar">
			<article>
				<h2>Middle sidebar</h2>
				<p>Lorum ipsum doloru in the middle</p>
			</article>
		</aside>

		<aside Class="bottom-sidebar">
			<article>
				<h2>Bottom sidebar</h2>
				<p>Lorum ipsum dolorum at the bottom</p>
			</article>
		</aside>

		<footer Class="mainFooter">
			<p>Copyright &copy; <a href="#" title = " 
	 MyDesign">mywebsite.com</a></p>
		</footer>

	</body>

	</html> 
	
**CSS is for the look and style!!**

### To style your website, create the style/style.css file in your root folder using the following code:

	/* 
		Stylesheet for:
		HTML5-CSS3 Responsive page
	*/

	/* body default styling */

	body {
		/* border: 5px solid red; */
		background-image: url(img/bg.png);
		background-color: #87bac4;
		font-size: 87.5%;
		/* base font 14px */
		font-family: Arial, 'Lucinda Sans Unicode';
		line-height: 1.5;
		text-align: left;
		margin: 0 auto;
		width: 70%;
		clear: both;
	}


	/* style the link tags */
	a {
		text-decoration: none;
	}

	a:link a:visited {
		color: #8B008B;
	}

	a:hover,
	a:active {
		background-color: #87bac4;
		color: #FFF;
	}


	/* define mainHeader image and navigation */
	.mainHeader img {
		width: 18%;
		height: auto;
		margin: 2% 0;
	}

	.mainHeader nav {
		background-color: #666;
		height: 40px;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.mainHeader nav ul {
		list-style: none;
		margin: 0 auto;
	}

	.mainHeader nav ul li {
		float: left;
		display: inline;
	}

	.mainHeader nav a:link,
	mainHeader nav a:visited {
		color: #FFF;
		display: inline-block;
		padding: 10px 25px;
		height: 20px;
	}

	.mainHeader nav a:hover,
	.mainHeader nav a:active,
	.mainHeader nav .active a:link,
	.mainHeader nav a:active a:visited {
		background-color: #8B008B;
		/* Color purple */
		text-shadow: none;
	}

	.mainHeader nav ul li a {
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}


	/* style the contect sections */

	.mainContent {
		line-height: 20px;
		overflow: none;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.content {
		width: 70%;
		float: left;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.articleContent {
		background-color: #FFF;
		padding: 3% 5%;
		margin-top: 2%;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.post-info {
		font-style: italic;
		color: #999;
		font-size: 85%;
	}

	.top-sidebar {
		width: 18%;
		margin: 1.5% 0 2% 2%;
		padding: 3% 5%;
		float: right;
		background-color: #FFF;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.middle-sidebar {
		width: 18%;
		margin: 1.% 0 2% 2%;
		padding: 3% 5%;
		float: right;
		background-color: #FFF;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.bottom-sidebar {
		width: 18%;
		margin: 1.5% 0 2% 2%;
		padding: 3% 5%;
		float: right;
		background-color: #FFF;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.mainFooter {
		width: 100%;
		height: 40px;
		float: left;
		border-color: #d99090;
		background-color:#666;
		margin: 2% 0;
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
	}

	.mainFooter p {
		width: 92%;
		margin: 10px auto;
		color: #FFF;
	}

**And then link the style-sheet like this:**  
![Screenshot](../img/iap/6.png)

## Page responsiveness to your website
To put in page responsiveness, meaning the page will be displayed different for each device/screen dimension add this line to the section.  
	<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
This line tells the browser that the width of the screen should be considered the "Full Width" of the page.
Meaning no matter the width of the device you are on, whether on desktop or mobile.
the website will follow the width of the device the user is on.
Responsiveness allows the webpage to be viewed differently on different devices.

After this the whole aspect of responsiveness is writtin in the CSS file. The strategy is to create the full CSS page first and after that create a section tag like the one shown  
	@Media only screen and (min-width:150px) and (max-width:600){ }
Than add display format specific styling. 
Just overrule the CSS tags u want different than standard.

	/* Add responsiveness overrules values for different screen resolution */

	@media only screen and (min-width:150px) and (max-width:600) {
		.body {
			width: 95%;
			font-size: 95%;
		}
		.mainHeader img {
			width: 100%;
		}
		.mainHeader nav {
			height: 160px;
		}
		.mainHeader nav ul {
			padding-left: 0;
		}
		.mainHeader nav ul li {
			width: 100%;
			text-align: center;
		}
		.mainHeader nav a:link,
		mainHeader nav a:visited {
			padding: 10px 25px;
			height: 20px;
			display: block;
		}
		.content {
			width: 100%;
			float: left;
			margin-top: 2%;
		}
		.post-info {
			display: none;
		}
		.topContent,
		.bottomContent {
			background-color: #FFF;
			padding: 3% 5%;
			margin-top: 2%;
			margin-bottom: 4%;
			border-radius: 5px;
			-moz-border-radius: 5px;
			-webkit-border-radius: 5px;
		}
		.top-sidebar,
		.middle-sidebar,
		.bottom-sidebar {
			width: 94%;
			margin: 2% 0 2% 0;
			padding: 2% 3%;
		}
	}

## Websocket

The WebSocket API is an advanced technology that makes it possible to open a two-way interactive communication session between the user's browser and a server. 
With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.

![Screenshot](../img/iap/7.png)

Some libaries we used:

1: aan/uit knop  
2: Chart  
3: Chat (websocket)  

*For nice buttons use the following libaries:*

• bootstrap.lib  
• JQuery.lib

*For the infra libary use websocket.js*

*For utilities use also the JQuery libary*

*To add a basic linechart to your website follow the next few steps:*
1: Download chart.js  
2: Add the chart.js to the folder  \public\js\lib\  
3: In the index.html add this code (this to host the chart inside your webpage)  
	<head>
    <script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>
	</head>

Add Article 2 at the bottom just above the body of the Javascript code to add the buttons for the chart:

	<div style="width:100%; height:100%">
	<canvas id="canvas"></canvas>
	<button id="randomizeData">Randomize Data</button>
	<button id="addDataset">Add Dataset</button>
	<button id="removeDataset">Remove Dataset</button>
	<button id="addData">Add Data</button>
	<button id="removeData">Remove Data</button>
	</div>

![Screenshot](../img/iap/8.png)

### utils.js
util.js contains a number of utility functions to help you update your web pages with javascript data (such as might be returned from the server). 
utils.js javascript code:

	'use strict';

	window.chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
	};

	(function(global) {
		var MONTHS = [
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		];

		var COLORS = [
			'#4dc9f6',
			'#f67019',
			'#f53794',
			'#537bc4',
			'#acc236',
			'#166a8f',
			'#00a950',
			'#58595b',
			'#8549ba'
		];

		var Samples = global.Samples || (global.Samples = {});
		var Color = global.Color;

		Samples.utils = {
			// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
			srand: function(seed) {
				this._seed = seed;
			},

			rand: function(min, max) {
				var seed = this._seed;
				min = min === undefined ? 0 : min;
				max = max === undefined ? 1 : max;
				this._seed = (seed * 9301 + 49297) % 233280;
				return min + (this._seed / 233280) * (max - min);
			},

			numbers: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 1;
				var from = cfg.from || [];
				var count = cfg.count || 8;
				var decimals = cfg.decimals || 8;
				var continuity = cfg.continuity || 1;
				var dfactor = Math.pow(10, decimals) || 0;
				var data = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = (from[i] || 0) + this.rand(min, max);
					if (this.rand() <= continuity) {
						data.push(Math.round(dfactor * value) / dfactor);
					} else {
						data.push(null);
					}
				}

				return data;
			},

			labels: function(config) {
				var cfg = config || {};
				var min = cfg.min || 0;
				var max = cfg.max || 100;
				var count = cfg.count || 8;
				var step = (max - min) / count;
				var decimals = cfg.decimals || 8;
				var dfactor = Math.pow(10, decimals) || 0;
				var prefix = cfg.prefix || '';
				var values = [];
				var i;

				for (i = min; i < max; i += step) {
					values.push(prefix + Math.round(dfactor * i) / dfactor);
				}

				return values;
			},

			months: function(config) {
				var cfg = config || {};
				var count = cfg.count || 12;
				var section = cfg.section;
				var values = [];
				var i, value;

				for (i = 0; i < count; ++i) {
					value = MONTHS[Math.ceil(i) % 12];
					values.push(value.substring(0, section));
				}

				return values;
			},
	*
			color: function(index) {
				return COLORS[index % COLORS.length];
			},

			transparentize: function(color, opacity) {
				var alpha = opacity === undefined ? 0.5 : 1 - opacity;
				return Color(color).alpha(alpha).rgbString();
			}
		};

		// DEPRECATED
		window.randomScalingFactor = function() {
			return Math.round(Samples.utils.rand(-100, 100));
		};

		// INITIALIZATION

		Samples.utils.srand(Date.now());

		// Google Analytics
		/* eslint-disable */
		if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-28909194-3', 'auto');
			ga('send', 'pageview');
		}
		/* eslint-enable */

	}(this));
	Add a button on the aside

	Button: button

	Make a button.css file in the CSS folder

	Add this code in it for the button

	.onoffswitch {
		position: relative; width: 90px;
		-webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	}
	.onoffswitch-checkbox {
		display: none;
	}
	.onoffswitch-label {
		display: block; overflow: hidden; cursor: pointer;
		height: 30px; padding: 0; line-height: 30px;
		border: 0px solid #807878; border-radius: 22px;
		background-color: #EEEEEE;
	}
	.onoffswitch-label:before {
		content: "";
		display: block; width: 22px; margin: 4px;
		background: #A1A1A1;
		position: absolute; top: 0; bottom: 0;
		right: 56px;
		border-radius: 22px;
		box-shadow: 0 6px 12px 0px #757575;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label {
		background-color: #292529;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
	   border-color: #292529;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
		margin-left: 0;
	}
	.onoffswitch-checkbox:checked + .onoffswitch-label:before {
		right: 0px; 
		background-color: #C91CAC; 
		box-shadow: 3px 6px 18px 0px rgba(0, 0, 0, 0.2);
	}

## JavaScript  
JavaScript was initially created to “make web pages alive”.
With NodeJS now also server-side or on any device that has a special program called the JavaScript engine.
It's a Functional Programming style.

**What can it do?**  
Well it can add new HTML to the page, change the existing content, modify styles.
It can also react to user actions such as *run on mouse clicks, pointer movements, key presses*.
Same goes for sending requests over the network to remote servers, downloading and uploading files *(so-called AJAX and COMET technologies)*, get and set cookies, ask questions to the visitor and show messages.
Remembering the data on the client-side (“local storage”) is one of the pros as well.

Further it can

• Directly talk to your computer or device functions  
• Is NOT allowed to communicate freely between sites  
• One browser window does NOT know of any other browser window/tab  
• Cross domain communication crippled  
• Be compiled so all code is visible to others  
• Is considered insecure  

### Using Object and Arrays through following code:

	Object={
			}
			
	And 

	Array = [ , , ]

	GetElementByTag

	= wordt
	== equal to
	+= was and new

	examples:
	Array: 
	var mygifs[ ];
	mygifs=["car","boat","bff"]
	if we choose to have "bff" then we write: mygifs(2);

	mygifs.shift (adds a value)

	We array: 
	1. shift
	2. push
	3. pop

	Object:
	var myObj{};
	value pairs
		={temp:"10",
			highTemp:[, ,],
			onclick:","
		}	

### Interconnect NodeJs to MQTT

**What is MQTT?**  
MQTT itself is a very simple publish / subscribe protocol. It allows you to send messages on a topic (you can think of these as channels) passed through a centralized message broker. 
The whole protocol is very lightweight on purpose. This makes it easy to run on embedded devices. Nearly every microcontroller has a library available for it to send and receive MQTT messages.

*Type the following console in the nodejs/project folder:*

	> sudo npm install --save mqtt
	
*Then edit the index.js by adding the following code:*

	var mqtt  = require('mqtt');
	var client  = mqtt.connect('mqtt://127.0.0.1');

	client.on('connect', function () {
	client.subscribe('#');
	client.publish('/', 'Connected to MQTT-Server');
	console.log("\nNodeJS Connected to MQTT-Server\n");
	});
	// send all messages from MQTT to the Websocket with MQTT topic
	client.on('message', function(topic, message){
	console.log(topic+'='+message);
	io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
	});

### Interconnect WebSocket with MQTT  
*Add the reverse from socket back to MQTT inside the SocketIO code:*

	io.sockets.on('connection', function (socket) {

	// Add to handle from Socket to MQTT:
	socket.on('mqtt', function (data) {
	console.log('Receiving for MQTT '+ data.topic + data.payload);
		// TODO sanity check .. is it valid topic ... check ifchannel is "mq$
	client.publish(data.topic, data.payload);
	});
	
### Creating a Web Widget.  

*What's a Web Widget?*  
A Web Widget is a web page or web application that is embedded as an element of a host web page but which is substantially independent of the host page
It is actually a small web-app inside another page that is embedded and sandboxed inside another page.
Most popular are the Dashlets or dashboard widgets.

To create a web widget follow the next steps:  
1: Create a folder /widgets  
2: Create folder /widgets/chartjs/ and create files: chartjs.js, chartjs.html & chartjs.css in there  
3: To  add

![Screenshot](../img/iap/9.png)

## The Scada Project  
Supervisory control and data acquisition (SCADA) is a control system architecture comprising computers, networked data communications and graphical user interfaces (GUI) for high-level process supervisory management, 
while also comprising other peripheral devices like programmable logic controllers (PLC) and discrete proportional-integral-derivative (PID) controllers to interface with process plant or machinery.
The use of SCADA has been considered also for management and operations of project-driven-process in construction.
Scada is used for giving commands from your browser via the socket to the RbPi.

*A socket is one endpoint of a two-way communication link between two programs running on the network. A socket is bound to a port number so that the TCP layer can identify the application that data is destined to be sent to.*  
*The socket works with nodejs.*

### General purpose io  
A general-purpose input/output (GPIO) is an uncommitted digital signal pin on an integrated circuit or electronic circuit board whose behavior—including whether it acts as input or output—is controllable by the user at run time. 
GPIOs have no predefined purpose and are unused by default.

Instead of using a Tagname, class or getElementById("theo").id (or getElementById("blue").class.
We use the "$" sign:
eg. $('# theo') or $('.blue')
Steps to  install the pigpio:

1. Type: sudo nodeapp.js  
2. Type: sudo apt-get install pigpio  
3. Type: npm install pigpio  

## Project Arduino Lcd Project

1. Display text from an arduino uno to a 16x2 lcd screen  
2. Add a sensor to the circuit and display the sensor data  
3. Add led to the circuit  
4. Add piezo (buzzer)  

**Using the pins of the RBPi for the 4 leds:**
1. pin 17 for 11  
2. pin 18 for 12  
3. pin 27 for 13  
4. pin 22 for 15  

	

	







  