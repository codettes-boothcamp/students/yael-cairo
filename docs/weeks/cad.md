# 3D printing / Cad design using different applications (online and offline)
**(CAD) is short for Computer Aided Design**
  
1. Practice making our 3D Designs in TinkerCad
2. We needed to instal Fusion 360
3. we also had to instal Inkscape
4. We printed our design with the 3D printer but first we installed and configured the 3D printer Anycubic 4Max pro

## ANYCUBIC 4MAX pro  

The Anycubic 4Max Pro 3D printer is an improved version of the original 4Max (2018).  
With a completely new look and target audience, the Anycubic 4Max Pro satisfies a segment of the market.  
By putting together a 3D printer that is both safe and easy to use, the latest iteration of the 4Max is geared toward the educational and entry-level market.  
Judging by its predecessor *(thing that has been followed or replaced by another)*, it’s pretty clear that Anycubic has taken a new and refined approach with its latest 3D printer.  

**ANYCUBIC 4MAX Pro**  
![Screenshot](../img/cad/pro.png)

## Autodesk TinkerCad  

Tinkercad is a free, online 3D modeling program that runs in a web browser, known for its simplicity and ease of use.  
Since it became available in 2011 it has become a popular platform for creating models for 3D printing as well as an entry-level introduction to constructive solid geometry in schools.  

**What's the concept of Tinkercad?**  
Tinkercad uses a simplified constructive solid geometry method of constructing models. A design is made up of primitive shapes that are either "solid" or "hole".  
Combining solids and holes together, new shapes can be created, which in turn can be assigned the property of solid or hole.  
In addition to the standard library of primitive shapes, a user can create custom shape generators using a built-in JavaScript editor.  
Shapes can be imported in three formats: STL and OBJ for 3D, and 2-dimensional SVG shapes for extruding into 3D shapes. Tinkercad exports models in STL or OBJ formats, ready for 3D printing.  
Tinkercad also includes a feature to export 3D models to Minecraft Java Edition, and also offers the ability to design structures using Lego bricks.  

## Inkscape  

Inkscape is a free and open-source vector graphics editor. This software can be used to create or edit vector graphics such as illustrations, diagrams,  
line arts, charts, logos, business cards, book covers, icons, CD/DVD covers, and complex paintings.  
Inkscape's primary vector graphics format is Scalable Vector Graphics (SVG); however, many other formats can be imported and exported.  
Inkscape can render primitive vector shapes (e.g. rectangles, ellipses, polygons, arcs, spirals, stars and 3D boxes) and text.  
These objects may be filled with solid colors, patterns, radial or linear color gradients and their borders may be stroked, both with adjustable transparency.  
Embedding and optional tracing of raster graphics is also supported, enabling the editor to create vector graphics from photos and other raster sources.  
Created shapes can be further manipulated with transformations, such as moving, rotating, scaling and skewing.  

## Fusion 360

Fusion 360 helps students and educators prepare for the future of design.  
It's the first 3D CAD, CAM, and CAE tool of its kind, connecting your entire product development process into one cloud-based platform.  
  
## Learning outcomes:
  
**Assessment:**  
Design your own 3D Project in TinkerCad
    
![Screenshot](../img/cad/01.png)

Design your own 3D Project in Fusion 360
  
![Screenshot](../img/cad/02.png)  
![Screenshot](../img/cad/a.png)  
![Screenshot](../img/cad/b.png)  
![Screenshot](../img/cad/c.png)  

  
**Get to know Fusion 360**  
The Fusion 360 is jut like the tinkercad, but has more advanced options. The environment is more specific, and a bit complexer.  
A few of the shortcuts in the Fusionn 360 app are:  
C - Circles  
R - Rectangles  
L - Line  
E - Extrude  
H - Hole  
M - Move  

We then got homework to try it ourselfs at home..



