# Embedded Programming  
**Learning the basics of programming**  
In this week we did Embedded Programming OBVIOUSLY. You would ask what embedded programming is, well here's a few things about embedded programming.  
Embedded programming is a specific type of programming that supports the creation of consumer facing or business facing devices that don't operate 
on traditional operating systems the way that full-scale laptop computers and mobile devices do.  
The idea of embedded programming is part of what drives the evolution of the digital appliances and equipment in today's IT markets.
*Embedded programming is also known as embedded software development or embedded systems programming.*

## Basic electronics  
We started with some basic electronics.  
There are a number of basic concepts that form the foundations of today's electronics technology. 
Current, voltage, resistance, capacitance, and inductance are a few of the basic elements of electronics.
Apart from these, there are many other interesting elements to electronic technology. While some can become quite complicated, 
it is nevertheless possible to gain a good understanding of them without delving into the complicated depths of these topics.

*Current (Electricity)*  
Current is the flow of an electric charge. It is an important quantity in electronic circuits. 
Current flows through a circuit when a voltage is placed across two points of a conductor.
Electricity flows in two ways: either in an alternating current (AC) or in a direct current (DC). 
The difference between AC and DC lies in the direction in which the electrons flow. In DC, the electrons flow steadily in a single direction, or "forward." 
In AC, electrons keep switching directions, sometimes going "forward" and then going "backward."
**Alternating current is the best way to transmit electricity over large distances.**
You have circuits that can be open(I) or close(O).
and you have series and parallel connection. You also have resistors, capacitors, diodes, transistors, potentiometers, LEDs, switches, wire, connectors etc.
For Embedded Programming we uses the Arduino Uno Kit and the Arduino 1.8.9 software. In the software we are writing in C-language and the coding is called sketch.
  
**Starting to work with arduino**  
  
### Arduino uno  
The first thing we did is install the Arduino app to start working with the Arduino. The first effect we used was the the blink effect.
To use this effect the steps we made were:  
1: File  
2: Examples  
3: Basic  
4: Blink  
![Screenshot](../img/31.png)  
  
**This is what the code looked like**  
![Screenshot](../img/26.png)  
  
The next thing we did was placing a little LED on the arduino, And made sure the light was blinking.  
  
Then we used our arduino to create a power rail on our breadboard. We did this from 