# Video production
**Learning the basics of Adobe Premiere**
## Week 2  

### Installing Adobe Premiere  
**I didn't have to install it because I already had it on my laptop, so I just had to open it..**  
![Screenshot](../img/20.png) 

### Opening Adobe Premiere  
![Screenshot](../img/21.png)  
  
### Creating a new project   
![Screenshot](../img/9.png)  
  
![Screenshot](../img/10.png)  

### Inserting new video  
![Screenshot](../img/11.png)  

### Removing the green background using ultra key  
![Screenshot](../img/17.png)  
![Screenshot](../img/25.png)  

### Put in another background  
![Screenshot](../img/14.png)  

### Added titles and used some effects  
![Screenshot](../img/19.png)  

### Exporting the video  
**Don't forget to select all the things you want to export (on your timeline)**  
![Screenshot](../img/22.png)  
  
![Screenshot](../img/23.png)  
  
![Screenshot](../img/24.png)






 