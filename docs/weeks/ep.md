# Electronics Production
This weeks learning aspects are:  
1: How to work with KICAD   
2: How to work with FlatCam  
3: How to import and append libaries into KiCad.

## Working with KICAD

*Step 1*

Make a new folder in local directory to I save all your work.  
![Screenshot](../img/try/1.png)

*Step 2*  

To make the first project go to  
•File  
•New  
•Project  

![Screenshot](../img/try/2.png)

Place various components in your workspace by selecting the components with their footprints.
The footprint gives the exact dimension of the component. If there are components missing, you have to import the libaries.
For the first project we made an Adruino uno in class, using the Atmega328p-Au as the processor.  

![Screenshot](../img/try/3.png)  
*To relocate the component, click on the component itself and not on the name of the components.*  
**When working with different componets (symbols) you will need the datasheet to see the specs of them. So you can see which pins you can connect to and which pins you can't connect to.**  

![Screenshot](../img/try/4.png)

### Placing the components (step 3)
To Place the components we used the 3rd icon on the right. If the window opens you may search for the component that you needed.  
*If the window doesn't open try pressing a random spot on your screen and that must open the window manually*  

![Screenshot](../img/try/5.png)  
![Screenshot](../img/try/6.png)  

After placing the componets (symbols), label them correctly.
To rotate a component, select it and then press "r".  

After placing all the symbols in the workspace, place the wires (green icon on the right) between the symbols to complete your KiCad Sketch.  

![Screenshot](../img/try/7.png)  

If you're finished with the sketch Annotate the sketch by clicking on  
•Tools  
•Annotate Schematic.  

![Screenshot](../img/try/10.png)  
			**or**  
By pressing annotate button in the toolbar  

![Screenshot](../img/try/8.png)  
![Screenshot](../img/try/9.png)  

Annotating is needed to see if your compoents or symbols are labeled correctly (No double naming).  

*Step 4*
This step is to assign the footprint.  
•Go to Tools  
•Assign Footprints  

![Screenshot](../img/KICAD/11.png)   

*Before you assign the footprint, add the fab mod libary from your local drive and then import the libary.*  

![Screenshot](../img/KICAD/b.png)  
![Screenshot](../img/KICAD/a.png)  

After assigning the footprint you can generate the netlist, but before generating the netlist you have to make a special folder in your local project folder. All netlists will be save in this folder.
To organise your project, place all netlists in your netlist folder on your local machine.
To generate the netlist click on the "Generate netlist" button on the toolbar.  

![Screenshot](../img/KICAD/c.png)  

Run the PCB New (Printed Circuit Board) by pressing that button on the toolbar.  

![Screenshot](../img/KICAD/d.png)  

You will get to see your PCB with all the components. Try to organize your components by placing the route tracks with the 5th icon on the right.  

![Screenshot](../img/KICAD/j.png)  

*Route Tracks are colored in RED.*
First organize your components:  

![Screenshot](../img/KICAD/k.png)  

Click on  
•Setup  
•Design Rules  
Do this to place the right Net Classes for the Clearance, Track Width and other things where needed.  

![Screenshot](../img/KICAD/p.png)  

After placing the Route tracks, place the Auxiliary Axus by clicking the *Drill and Place an Offset* button in the tool panel or you can also go to Place and click the same button.  

![Screenshot](../img/KICAD/q.png)  

When you did that you can see the Dimension: 44.450 mm x 60.960 mm  

![Screenshot](../img/KICAD/n.png)  

### Plotting
After making the Route tracks, plot the schematic by clicking  
•File  
•Plot  

![Screenshot](../img/KICAD/l.png)  
![Screenshot](../img/KICAD/m.png)  

## FlatCam
Place all parameters and then save the file as an gbr-file  

![Screenshot](../img/ep/1.png)  
![Screenshot](../img/ep/2.png)  










